(function ($) {
    $(window).on("load", function (e) {
        "use strict";

        $('#rate-it').barrating({
            theme: 'fontawesome-stars'
        });

    });
})(jQuery);
