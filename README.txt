=== Stars Rating ===
Contributors: fahidjavid
Tags: comments, rating, reviews, stars rating, comments vote, voting, shortcode
Requires at least: 3.0
Tested up to: 5.0
Stable tag: 5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

A simple and easy to use plugin that turns post, pages and custom post types comments into reviews.

Its main features are as follows:

* Turns post, pages and custom post types comments into reviews.
* Allows you to choose post types on which you want to enable Stars Rating feature.
* Also, allows you to enable/disable stars rating feature for the posts and pages individually.
* Offers a shortcode <strong>[star_rating]</strong> to display average rating anywhere in the post/page

== Installation ==

1. After installing the plugin go to the <strong>Settings > Discussion</strong> page (at the very bottom) and enable desired post types for the Stars Rating.
1. That's it :)

== Screenshots ==

1. Enable/Disable 'Stars Rating' for the posts, pages and custom post types comments globally.
2. Enable/Disable 'Stars Rating' for the posts, pages and custom post types comments individually.
3. Comments with their ratings and an average rating above comments.
4. Rating option in comment form.
5. Shortcode <strong>[star_rating]</strong> to display average rating anywhere in the post/page

== Changelog ==

= 1.3.1 =
* Fixed styling issue

= 1.3.0 =
* Tested with WordPress 5.0
* Fixed the default rating value

= 1.2.0 =

* Added show/hide support for average rating above comments section
* Added "[star_rating]" shortcode support to display average rating in the post/page content area or loop
* Set the rating option in the comment form to 5 stars by default

= 1.1.0 =

* Tested plugin up to WP V4.9.8

= 1.0.0 =

* Initial Release