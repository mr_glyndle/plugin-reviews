<?php
/*
Plugin Name: 			On site Reviews
Description: 			Turn comments into reviews with structured schema data. Requires Sitebooster Theme
Version:           		1.0.1
Author:            		Glyn Harrison
Text Domain: 			tbc-reviews
Domain Path: 			languages
License:           		GNU General Public License v2
License URI:       		http://www.gnu.org/licenses/gpl-2.0.html
Plugin URI: 			https://bitbucket.org/thebrandchap/plugin-reviews/
Bitbucket Plugin URI: 	https://bitbucket.org/thebrandchap/plugin-reviews/
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! function_exists( 'stars_rating_load_textdomain' ) ) {
    /**
     * Load text domain for translation.
     * @since 1.0.0
     */
    function stars_rating_load_textdomain() {
        load_plugin_textdomain( 'tbc-reviews', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    // load text domain for translation.
    stars_rating_load_textdomain();
}


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/tbc-reviews-include.php';

/**
 * Main instance of Stars_Rating.
 *
 * Returns the main instance of Stars_Rating to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return Stars_Rating
 */
function Stars_Rating(){
    return Stars_Rating::instance();
}

// Get Stars_Rating Running.
Stars_Rating();
